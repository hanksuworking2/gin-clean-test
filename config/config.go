package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

type Config struct {
	Debug    bool
	Server   ServerConfig
	Database DatabaseConfig
}

type ServerConfig struct {
	Address string
	TLS     bool
	Cert    string
	PriKey  string
}

type DatabaseConfig struct {
	MySQL   *MySQLConfig
	MongoDB *MongoDBConfig
	Redis   *RedisConfig
	MinIO   *MinIOConfig
}

type MySQLConfig struct {
	Host     string
	Port     int32
	User     string
	Password string
	Name     string
}

type MongoDBConfig struct {
	Host         string
	Port         int32
	User         string
	Password     string
	DatabaseName string
}

type RedisConfig struct {
	Host     string
	Port     int32
	Password string
	DB       int
}

type MinIOConfig struct {
	Endpoint   string
	AccessKey  string
	SecretKey  string
	Secure     bool
	BucketName string
}

var config Config

func init() {
	viper.SetConfigName("config")
	viper.SetConfigType("toml")

	// add currentPath to be the ConfigPath
	currentPath, _ := os.Getwd()
	viper.AddConfigPath(currentPath)

	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file: %s\n", err)
		return
	}

	if err := viper.Unmarshal(&config); err != nil {
		fmt.Printf("Error unmarshaling config file: %s\n", err)
		return
	}
}

func GetConfig() *Config {
	return &config
}
