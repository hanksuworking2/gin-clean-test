package database

import (
	"fmt"

	"go-utils/database/minio"
	"go-utils/database/mongo"
	"go-utils/database/redis"
	"go-utils/database/sql"
	zapLogger "go-utils/logger"

	"gin-clean-test/config"
)

type DataBaseConnCtrl struct {
	minio_conn *minio.MinioClient
	mongo_conn *mongo.DataBaseConn
	mysql_conn *sql.DataBaseConn
	redis_conn *redis.RedisClient
}

func ConnectToDB(logger *zapLogger.Logger) (*DataBaseConnCtrl, error) {
	var dbc DataBaseConnCtrl

	if config.GetConfig().Database.MySQL != nil {

		// mysql connection (database/sql)
		addr := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", config.GetConfig().Database.MySQL.User,
			config.GetConfig().Database.MySQL.Password, config.GetConfig().Database.MySQL.Host, config.GetConfig().Database.MySQL.Port, config.GetConfig().Database.MySQL.Name)

		dbConfig := &sql.DBConfig{
			DriverName:     sql.MYSQL,
			DataSourceName: addr,
		}

		dbConn, err := dbConfig.Connect()
		if err != nil {
			logger.Fatal("failed to open MySQL", zapLogger.String("error", err.Error()))
			return nil, err
		}

		dbc.mysql_conn = dbConn
	}

	if config.GetConfig().Database.Redis != nil {
		cfg := &redis.RedisConfig{
			Host:     config.GetConfig().Database.Redis.Host,
			Port:     config.GetConfig().Database.Redis.Port,
			Password: config.GetConfig().Database.Redis.Password,
			DB:       config.GetConfig().Database.Redis.DB,
		}

		redisClient, err := cfg.Connect()
		if err != nil {
			logger.Fatal("failed to open Redis", zapLogger.String("error", err.Error()))
			return nil, err
		}

		dbc.redis_conn = redisClient
	}

	if config.GetConfig().Database.MinIO != nil {
		minioClient, err := minio.New(&minio.MinioConfig{
			Endpoint:   config.GetConfig().Database.MinIO.Endpoint,
			AccessKey:  config.GetConfig().Database.MinIO.AccessKey,
			SecretKey:  config.GetConfig().Database.MinIO.SecretKey,
			UseSSL:     config.GetConfig().Database.MinIO.Secure,
			BucketName: config.GetConfig().Database.MinIO.BucketName,
		})

		if err != nil {
			logger.Fatal("failed to open MinIO", zapLogger.String("error", err.Error()))
			return nil, err
		}

		dbc.minio_conn = minioClient

	}

	if config.GetConfig().Database.MongoDB != nil {
		dbConfig := &mongo.MongoConfig{
			Host:     config.GetConfig().Database.MongoDB.Host,
			Port:     config.GetConfig().Database.MongoDB.Port,
			Username: config.GetConfig().Database.MongoDB.User,
			Password: config.GetConfig().Database.MongoDB.Password,
			Database: config.GetConfig().Database.MongoDB.DatabaseName,
		}

		dbConn, err := dbConfig.Connect()
		if err != nil {
			logger.Fatal("failed to open MongoDB", zapLogger.String("error", err.Error()))
			return nil, err
		}

		dbc.mongo_conn = dbConn
	}

	return &dbc, nil
}

func (ctrl *DataBaseConnCtrl) Close(logger *zapLogger.Logger) {
	if ctrl.mongo_conn != nil {
		logger.Info("mongodb", zapLogger.String("state", "connection close"))
		ctrl.mongo_conn.Close()
	}

	if ctrl.minio_conn != nil {
		logger.Info("minio", zapLogger.String("state", "connection close"))
		// ctrl.minio_conn.Close()
	}

	if ctrl.mysql_conn != nil {
		logger.Info("mysql", zapLogger.String("state", "connection close"))
		ctrl.mysql_conn.Close()
	}

	if ctrl.redis_conn != nil {
		logger.Info("redis", zapLogger.String("state", "connection close"))
		ctrl.redis_conn.Close()
	}

}

func (ctrl *DataBaseConnCtrl) MinIO() *minio.MinioClient {
	return ctrl.minio_conn
}

func (ctrl *DataBaseConnCtrl) MongoDB() *mongo.DataBaseConn {
	return ctrl.mongo_conn
}

func (ctrl *DataBaseConnCtrl) MySQL() *sql.DataBaseConn {
	return ctrl.mysql_conn
}

func (ctrl *DataBaseConnCtrl) Redis() *redis.RedisClient {
	return ctrl.redis_conn
}
