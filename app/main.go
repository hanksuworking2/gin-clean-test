package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	zapLogger "go-utils/logger"

	"gin-clean-test/config"
	"gin-clean-test/database"
	delivery "gin-clean-test/delivery/http"
)

var (
	appName   = "%APPNAME%"
	commitID  = "%COMMITID%"
	buildTime = "%BUILDID%"
	version   = "%VERSION%"
)

func init() {
	buildVer := false
	flag.BoolVar(&buildVer, "version", false, "print build version and then exit")
	flag.Parse()

	if buildVer {
		fmt.Printf("App Name:   %-30s\n", appName)
		fmt.Printf("Built Time: %-30s\n", buildTime)
		fmt.Printf("Git Commit: %-30s\n", commitID)
		fmt.Printf("Go Version: %-30s\n", version)
		os.Exit(0)
	}
}

func main() {
	/* TODO: context management */
	// parent context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger := zapLogger.New(os.Stderr, zapLogger.InfoLevel, zapLogger.DEV)

	dbc, err := database.ConnectToDB(logger)
	if err != nil {
		os.Exit(1)
	}

	router := delivery.NewHTTPRouter(dbc, logger)

	srv := &http.Server{
		Addr:    config.GetConfig().Server.Address,
		Handler: router,
	}

	/* TODO: daemon */
	/* ............ */

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Fatal("listen", zapLogger.String("error", err.Error()))
		}
	}()

	/* graceful showdown */

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// syscall.SIGTERM -> kill
	// syscall.SIGINT -> kill -2
	// syscall.SIGKILL -> kill -9, but it can't be catch, so ignore it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	logger.Info("Shutdown Server...")

	// close all database connections
	dbc.Close(logger)

	srvCtx, _ := context.WithTimeout(ctx, 5*time.Second)

	if err := srv.Shutdown(srvCtx); err != nil {
		logger.Fatal("failed to shutdown server", zapLogger.String("error", err.Error()))
	}

	logger.Info("Server shutdown successfully")
}
