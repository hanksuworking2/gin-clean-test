package domain

import (
	gonull "go-utils/go-nullable"
)

type DeviceStatus int

const (
	// 0: offline
	OFFLINE = iota
	// 1: online
	ONLINE
)

type Edge struct {
	Id       string // Device Id
	OS       string
	Name     string // Device Name
	IsOnline DeviceStatus
	Models   []*EdgeModel
}

type EdgeModel struct {
	ModelId      gonull.NullString
	ModelName    gonull.NullString
	ModelVersion gonull.NullString //Model版本，x,y,z
	Trained      int32             // 此Model版本的第幾次訓練
}

type EdgeRepository interface {
	Get(id string) ([]*Edge, error)
	// AddToDeviceCmdTable(action, deviceId, data string) (payload string, err error)
}

type EdgeUsecase interface {
	EdgeDeviceDetailApi(id string) ([]*Edge, error)
	EdgeDeployApi(devices []string, modelId, modelVersion string, trained int32) error
}
