package domain

import (	
	"time"

	gonull "go-utils/go-nullable"
)

type UserRole int32

const (
	// 0 SUPER USER
	SYSTEM_ADMIN UserRole = iota
	// 1 平台管理者
	PLATFORM_ADMIN
	// 2 VIA 員工
	STAFF
	// 3 代理商
	AGENT
	// 4 客戶
	CUSTOMER
)

type UserStatusCode int32

const (
	// 0 disable
	DISABLE UserStatusCode = iota
	// 1 enable
	ENABLE
)

func CodeCheck(check any, target int32) bool {
	switch check.(type) {
	case UserRole:
		if target < 0 || target > int32(CUSTOMER) {
			return false
		}
	case UserStatusCode:
		if target < 0 || target > int32(ENABLE) {
			return false
		}

	}

	return true
}

type User struct {
	ID         int32
	Name       string
	Role       UserRole
	Email      string
	Password   string
	Age        gonull.NullInt32
	Status     UserStatusCode
	Avatar_URL string // From MinIO
	Created_at time.Time
	Updated_at time.Time
}

type UserRepository interface {
	Create(user *User) error
	FindByEmail(email string) (*User, error)
	FindByUsername(username string) (*User, error)
	Update(email string, params *gonull.Nullable) error
}

type UserUseCase interface {
	Register(user *User) error
	Login(email, password string) (*Token, error)
	UpdateUser(email string, reqBody *gonull.Nullable) error
}
