package domain

type Token struct {
	Token string
	User  string
}

var TokenExpire = 1800 //預設Token有效時間 1800秒

type TokenRepository interface {
	SaveToken(token *Token) error
	GetToken(user string) (*Token, error)
}
