package domain

import "time"

type RequestInfo struct {
	Method      string
	URI         string
	Headers     map[string][]string
	RequestBody string
	StatusCode  int
	Latency     float64
	CreatedAt   time.Time
}
