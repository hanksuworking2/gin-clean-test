package mysql

import (
	"database/sql"
	"errors"
	"fmt"

	gonull "go-utils/go-nullable"

	"gin-clean-test/domain"
)

type mysqlUserRepository struct {
	Conn *sql.DB
}

// NewMysqlUserRepository will create an object that represent the User.Repository interface
func NewMysqlUserRepository(conn *sql.DB) domain.UserRepository {
	return &mysqlUserRepository{conn}
}

func (m *mysqlUserRepository) Create(user *domain.User) (err error) {
	insertSQL := "INSERT INTO users (Name, Role, Email, Password, Age, Status, Avatar_URL) VALUES (?, ?, ?, ?, ?, ?, ?)"
	_, err = m.Conn.Exec(insertSQL, user.Name, user.Role, user.Email, user.Password, user.Age.Value(), user.Status, user.Avatar_URL)
	return
}

func (m *mysqlUserRepository) FindByEmail(email string) (user *domain.User, err error) {
	querySQL := "SELECT * FROM users WHERE Email = ?"
	row := m.Conn.QueryRow(querySQL, email)

	var u domain.User
	err = row.Scan(&u.ID, &u.Name, &u.Role, &u.Email, &u.Password, &u.Age, &u.Status, &u.Avatar_URL, &u.Created_at, &u.Updated_at)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, domain.ErrNotFound
		}
		return nil, err
	}

	return &u, nil
}

func (m *mysqlUserRepository) FindByUsername(username string) (user *domain.User, err error) {
	querySQL := "SELECT * FROM users WHERE Name = ?"
	row := m.Conn.QueryRow(querySQL, username)

	var u domain.User
	err = row.Scan(&u.ID, &u.Name, &u.Role, &u.Email, &u.Password, &u.Age, &u.Avatar_URL, &u.Created_at, &u.Updated_at)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, domain.ErrNotFound
		}
		return nil, err
	}

	return &u, nil
}

func (m *mysqlUserRepository) Update(email string, params *gonull.Nullable) (err error) {
	querySQL := "UPDATE users SET "
	var values []interface{}

	for key, value := range *params {
		querySQL += fmt.Sprintf("%s = ?, ", key)
		values = append(values, value)
	}
	
	// remove the last comma and space
	querySQL = querySQL[:len(querySQL)-2]

	// add other necessary conditions
	querySQL += " WHERE Email = ?"
	values = append(values, email)

	fmt.Println("Query:", querySQL)
	fmt.Println("Values:", values)

	_, err = m.Conn.Exec(querySQL, values...)

	return
}
