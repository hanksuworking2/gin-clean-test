package mysql

import (
	"database/sql"
	"fmt"
	
	gonull "go-utils/go-nullable"

	"gin-clean-test/domain"
)

type MysqlEdgeRepository struct {
	Conn *sql.DB
}

// NewMysqlUserRepository will create an object that represent the User.Repository interface
func NewMysqlEdgeRepository(conn *sql.DB) domain.EdgeRepository {
	return &MysqlEdgeRepository{conn}
}

func (m *MysqlEdgeRepository) Get(id string) (edges []*domain.Edge, err error) {

	sqlScript := `
		Select
			d.Id,
			d.OS,
			d.Name,
			do.IP,
			IFNULL(do.IsOnline,0) as IsOnline,
			dm.ModelId,
			m.Name as ModelName,
			dm.ModelVersion,
			dm.Trained,
			dm.Version
		from 
			Device d
		left join
			DeviceModel dm
		on
			d.Id = dm.DeviceId
			and
			CHAR_LENGTH(dm.Version)>0
		left join
			Model m
		on
			m.Id = dm.ModelId
		left JOIN
			DeviceOnline as do
		ON
			d.Id = do.DeviceId
		where
			1
			and
			d.Id = ?
		;
		`

	rows, err := m.Conn.Query(sqlScript, id)
	if err != nil {
		return nil, fmt.Errorf("failed to query Edge information with id [%s]", id)
	}
	defer rows.Close()

	// Only Models will change, so here we can create the object outside the for loop
	var edge domain.Edge

	for rows.Next() {
		// Since we are using 'join', we need to handle the NULL value
		// Otherwise, we will easily receive the error "converting NULL to string is unsupported", in this case, modelName will probably encounter this error
		// In this case, I use pointer value to solve this issue
		edgeModel := &domain.EdgeModel{}
		var ip, version string		

		err = rows.Scan(&edge.Id, &edge.OS, &edge.Name, &ip, &edge.IsOnline, &edgeModel.ModelId, &edgeModel.ModelName, &edgeModel.ModelVersion, &edgeModel.Trained, &version)
		if err != nil {
			return nil, fmt.Errorf("failed to scan query, err=%s", err)
		}

		if !edgeModel.ModelId.Valid {
			continue
		}

		// ModelId is not emtpy, append EdgeModel to Edge
		if !edgeModel.ModelName.Valid {
			edgeModel.ModelName = edgeModel.ModelId
		}		
		edgeModel.ModelName = gonull.String(fmt.Sprintf("%s-%s", edgeModel.ModelName.String, version)) 

		if !edgeModel.ModelName.Valid {
			edgeModel.ModelName = edgeModel.ModelId
		}
		edge.Models = append(edge.Models, edgeModel)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to iterate the requirement, err=%s", err)
	}

	edges = append(edges, &edge)

	return
}
