package redis

import (
	"context"
	"encoding/json"		
	"time"

	"go-utils/database/redis"

	"gin-clean-test/domain"
)

type RedisTokenRepository struct {
	client *redis.RedisClient	
}

/* 
TODO: 
	Need to figure out how to manage the parent context and child context.
	Now, use context.TODO() to handle the operations of redis.Client.
*/

func NewRedisTokenRepository(client *redis.RedisClient) domain.TokenRepository {	
	return &RedisTokenRepository{client: client}
}

func (r *RedisTokenRepository) GetToken(user string) (*domain.Token, error) {	
	data, err := r.client.Client().Get(context.TODO(), user).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, err
	}

	var token domain.Token
	err = json.Unmarshal([]byte(data), &token)
	if err != nil {
		return nil, err
	}

	return &token, nil

}

func (r *RedisTokenRepository) SaveToken(token *domain.Token) error {
	data, err := json.Marshal(token)
	if err != nil {
		return err
	}

	return r.client.Client().SetEX(context.TODO(), token.Token, data, time.Second*time.Duration(domain.TokenExpire)).Err()

}
