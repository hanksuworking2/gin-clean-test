# gin-clean-test

## Concept
A crucial concept of Clean Architecture is to keep the business logic within the system independent of:

- Independent of frameworks: Treat frameworks as tools, not as places to squeeze the system into.

- Testable: Enable the testing of business logic in isolation, without dependencies on UI, databases, or web servers.

- Independent of UI: Allow changes to the UI without altering the system's architecture, even enabling direct interaction via a console window for these business logic operations.

- Independent of the database: Ensure that business logic remains separate from the database, so changing the database does not impact the operation of the system's business logic.

- Independent of any external agency: Enable the business logic to operate independently of any third-party services.

## Layers
- Entities: Similar to the Model layer, also known as the Domain layer, this defines the struct of the model and specifies the methods to be implemented through interfaces. These pre-defined entities are used across different layers.

- Repository: Similar to the database layer, responsible for database operations like CRUD without any business logic. It relies on the defined interfaces in the Domain layer, allowing database switches without issues. This layer depends on other database or microservice services for data exchange.

- Usecase: Comparable to the API or controller layer, primarily handling business logic such as data processing or computation. In this layer, the choice of repository is determined, and data is passed to the delivery or repository. It utilizes the methods provided by the Repository for actual database operations. Due to the interfaces defined in the Domain layer, this can be applied to various services (e.g., gRPC).

- Delivery: Resembling the router layer, it is primarily responsible for determining how data should be presented through different media, such as Restful API, gRPC, or HTML files. This layer accepts user-provided data, sanitizes it, and passes it to the usecase layer.