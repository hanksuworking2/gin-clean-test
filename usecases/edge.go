package usecases

import (
	"fmt"
	
	domain "gin-clean-test/domain"
)

type EdgeUsecase struct {
	EdgeRepository domain.EdgeRepository
}

func NewEdgeUsecase(edgeRepo domain.EdgeRepository) *EdgeUsecase {
	return &EdgeUsecase{EdgeRepository: edgeRepo}
}

func (usecase *EdgeUsecase) EdgeDeviceDetailApi(id string) (edges []*domain.Edge, err error) {
	if id == "" {
		return nil, fmt.Errorf("please specify the id")
	}

	return usecase.EdgeRepository.Get(id)
}

func (usecase *EdgeUsecase) EdgeDeployApi(devices []string, modelId, modelVersion string, trained int32) (err error) {
	return
}
