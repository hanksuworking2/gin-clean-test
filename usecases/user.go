package usecases

import (
	"fmt"
	"strings"

	"github.com/google/uuid"

	"go-utils/cryptor"
	gonull "go-utils/go-nullable"
	"go-utils/utils"

	domain "gin-clean-test/domain"
)

type UserUsecase struct {
	UserRepository  domain.UserRepository
	TokenRepository domain.TokenRepository
}

func NewUserUsecase(userRepo domain.UserRepository, tokenRepo domain.TokenRepository) *UserUsecase {
	return &UserUsecase{UserRepository: userRepo, TokenRepository: tokenRepo}
}

func (usecase *UserUsecase) Register(newUser *domain.User) (err error) {
	user, err := usecase.UserRepository.FindByEmail(newUser.Email)
	if err != nil && !strings.Contains(err.Error(), "not found") {
		return fmt.Errorf("failed to check user, err=%s", err)
	}

	if user != nil {
		return fmt.Errorf("user already exists")
	}

	// password to hash
	hashed_passwd, err := cryptor.GenPassword(newUser.Password)
	if err != nil {
		return fmt.Errorf("failed to generate password, err=%s", err)
	}
	newUser.Password = hashed_passwd

	// RecordInfo
	// newUser.Created_at = time.Now()
	// newUser.Updated_at = newUser.Created_at

	return usecase.UserRepository.Create(newUser)

}

func (usecase *UserUsecase) Login(email, password string) (tokenObj *domain.Token, err error) {
	// check email format
	if !utils.IsEmailValid(email) {
		return nil, fmt.Errorf("your email is incorrect")
	}

	// check if the user exists
	user, err := usecase.UserRepository.FindByEmail(email)
	if err != nil {
		return nil, fmt.Errorf("failed to find email, err=%s", err)
	}

	// password comparison
	isValid, err := cryptor.VerifyPassword(user.Password, password)
	if err != nil {
		return nil, fmt.Errorf("failed to verify password, err=%s", err)
	}

	if !isValid {
		return nil, fmt.Errorf("wrong password")
	}

	// setup new auth session
	tokenObj = &domain.Token{
		Token: uuid.New().String(),
		User:  user.Name,
	}

	return tokenObj, usecase.TokenRepository.SaveToken(tokenObj)
}

func (usecase *UserUsecase) UpdateUser(email string, reqBody *gonull.Nullable) (err error) {
	// check email format
	if !utils.IsEmailValid(email) {
		return fmt.Errorf("email is incorrect")
	}

	// check if the user exists
	_, err = usecase.UserRepository.FindByEmail(email)
	if err != nil {
		return fmt.Errorf("failed to find email, err=%s", err)
	}

	// check the supported fields, prune parameters
	reqBody.Prune([]string{"name", "role", "email", "password", "age", "status"})

	if reqBody.IsSet("Password") == gonull.EXIST {
		passwd := reqBody.GetString("Password")
		if passwd == "" {
			return fmt.Errorf("cannot specify an empty password or must be string")
		}

		// password to hash
		hashed_passwd, _ := cryptor.GenPassword(passwd)
		reqBody.Set("Password", hashed_passwd)
	}

	/*
		TODO: here needs an access control check
		i.e. probably Role and Status can only be modified by administrators
		if user.IsAdmin {
			// do stuff...
		}
	*/

	// Update the role only if it's of integer type.
	role, err := reqBody.GetInt64("Role");
	if err != nil || !domain.CodeCheck(domain.UserRole(1), int32(role)) {
		// reqBody.Set("Role", user.Role)
		return fmt.Errorf("make sure you have specified the right role")
	}

	// Update the status only if it's of integer type.
	status, err := reqBody.GetInt64("Status");
	if err != nil || !domain.CodeCheck(domain.UserStatusCode(1), int32(status)) {
		// reqBody.Set("Status", user.Role)
		return fmt.Errorf("make sure you have specified the right status")
	}

	return usecase.UserRepository.Update(email, reqBody)
}
