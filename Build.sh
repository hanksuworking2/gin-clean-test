#!/bin/bash

os=$(go env GOOS)

exe_name="clean-arch-service"

if [ "$os" == "windows" ]; then
    exe_name="clean-arch-service.exe"
fi

buildTime=$(date --rfc-3339=seconds)
commitID=$(git rev-parse HEAD)
go_version=$(go version)

app_name="$exe_name"

go build -o "$exe_name" -ldflags "-X 'main.buildTime=$buildTime' -X 'main.commitID=$commitID' -X 'main.appName=$app_name' -X 'main.version=$(go version)'" ./app/