package middleware

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	zapLogger "go-utils/logger"

	"gin-clean-test/domain"
)

func RequestLoggerMiddleware(logger *zapLogger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		startTime := time.Now()
		
		// Create a buffer to hold the request body data
		var requestBodyBuffer bytes.Buffer
		teeReader := io.TeeReader(c.Request.Body, &requestBodyBuffer)

		// Read the request body data
		jsonData, err := io.ReadAll(teeReader)
		if err != nil {
			// Handle the error
			logger.Error("Failed to read request body", zap.Error(err))
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		// Replace the original request body with the teeReader
		c.Request.Body = io.NopCloser(&requestBodyBuffer)

		// jump over to other middlewares until the operation is finished, then go back here to record the request information
		c.Next()

		elapsedTime := time.Since(startTime)

		requestInfo := domain.RequestInfo{
			Method:      c.Request.Method,
			URI:         c.Request.RequestURI,
			Headers:     c.Request.Header,
			RequestBody: string(jsonData),
			StatusCode:  c.Writer.Status(),
			Latency:     elapsedTime.Seconds(),
			CreatedAt:   time.Now(),
		}

		fmt.Printf("Request: %#v\n", requestInfo)

		// if err := dbConn.SaveRequestInfo(requestInfo); err != nil {
		// 	fmt.Printf("Failed to save request info to database: %v\n", err)
		// }
	}
}
