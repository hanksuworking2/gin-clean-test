package middleware

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		config := cors.DefaultConfig()
		config.AllowAllOrigins = true
		cors.New(config)
		
		// cors.New(cors.Config{
		// 	AllowOrigins:     []string{"https://foo.com"},
		// 	AllowMethods:     []string{"PUT", "PATCH"},
		// 	AllowHeaders:     []string{"Origin"},
		// 	ExposeHeaders:    []string{"Content-Length"},
		// 	AllowCredentials: true,
		// 	AllowOriginFunc: func(origin string) bool {
		// 		return origin == "https://github.com"
		// 	},
		// 	MaxAge: 12 * time.Hour,
		// })
	}
}
