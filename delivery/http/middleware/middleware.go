package middleware

import (
    "github.com/gin-gonic/gin"

	zapLogger "go-utils/logger"	
)

func NeedAuthenticateHandler(logger *zapLogger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		logger.Info("this is NeedAuthenticateHandler from middleware")
		c.Next()
	}

	/*
			 return func(ctx *gin.Context) {
		        rdb := NewRedisClient()
		        defer rdb.Close()

		        // Get "X-User-ID from http header
		        userID, err := strconv.Atoi(ctx.GetHeader("X-User-ID"))
		        if err != nil {
		            ctx.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid user ID"})
		            ctx.Abort()
		            return
		        }

		        // Check if the session is valid from Redis
		        sessionJSON, err := rdb.Get(ctx, fmt.Sprintf("session:%d", userID)).Result()
		        if err != nil {
		            ctx.JSON(http.StatusUnauthorized, gin.H{"error": "Session not found"})
		            ctx.Abort()
		            return
		        }

		        // parse session structure
		        var sessionData map[string]interface{}
		        if err := json.Unmarshal([]byte(sessionJSON), &sessionData); err != nil {
		            ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to parse session data"})
		            ctx.Abort()
		            return
		        }

		        // store user_id & username to the context
		        ctx.Set("user_id", int(sessionData["user_id"].(float64)))
		        ctx.Set("username", sessionData["username"].(string))

		        ctx.Next()
		    }

	*/
}

func NonAuthenticateHandler(logger *zapLogger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		logger.Info("this is NonAuthenticateHandler from middleware")
		c.Next()
	}
}
