package request

type BaseRequestInfo struct {
	Id   string `json:",omitempty"`
	Name string `json:",omitempty"`
}

// type UserRequest struct {
// 	BaseRequestInfo
// 	Name     gonull.NullString
// 	Role     gonull.NullInt32
// 	Email    gonull.NullString
// 	Password gonull.NullString
// 	Age      gonull.NullInt32
// 	Status   gonull.NullInt32
// }
