package http

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	gonull "go-utils/go-nullable"
	zapLogger "go-utils/logger"

	"gin-clean-test/delivery/http/response"
	"gin-clean-test/domain"
)

type UserController struct {
	UserUseCase domain.UserUseCase

	Logger *zapLogger.Logger
}

func NewUserController(userUseCase domain.UserUseCase, logger *zapLogger.Logger) *UserController {
	return &UserController{UserUseCase: userUseCase, Logger: logger}
}

func (ctrl *UserController) Register(ctx *gin.Context) {
	var userRegistrationInfo gonull.Nullable

	if rawData, err := ctx.GetRawData(); err != nil {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the your body")
		return
	} else {
		err = userRegistrationInfo.DecodeFromJSON(string(rawData))
		if err != nil {
			response.ErrorResponse(ctx, http.StatusBadRequest, err.Error())
			return
		}
	}

	// check the fields that needs to be provided
	if userRegistrationInfo.IsSet("Name") < gonull.EXIST {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the user name")
		return
	}

	if userRegistrationInfo.IsSet("Email") < gonull.EXIST {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the email")
		return
	}

	if userRegistrationInfo.IsSet("Password") < gonull.EXIST {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the email")
		return
	}

	// Verify the presence of Role and Status; if they are not specified, assign default values.
	var role domain.UserRole
	if userRegistrationInfo.IsSet("Role") < gonull.EXIST {
		role = domain.CUSTOMER
	} else {
		roleNumber, _ := userRegistrationInfo.GetInt32("Role")
		role = domain.UserRole(roleNumber)
	}

	var status domain.UserStatusCode
	if userRegistrationInfo.IsSet("Status") < gonull.EXIST {
		status = domain.ENABLE
	} else {
		statusNumber, _ := userRegistrationInfo.GetInt32("Status")
		status = domain.UserStatusCode(statusNumber)
	}

	var age gonull.NullInt32
	if userRegistrationInfo.IsSet("Age") == gonull.EXIST {
		age.Valid = true
		ageNubmer, _ := userRegistrationInfo.GetInt32("Age")
		age.Int32 = ageNubmer
	}

	// create domain.User, and pass it into the UserUseCase
	user := domain.User{
		Name:     userRegistrationInfo.Get("Name").(string),
		Role:     role,
		Email:    strings.ToLower(userRegistrationInfo.Get("Email").(string)),
		Password: userRegistrationInfo.Get("Password").(string),
		Age:      age,
		Status:   status,
	}

	err := ctrl.UserUseCase.Register(&user)
	if err != nil {
		response.ErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(ctx, nil)
}

func (ctrl *UserController) Login(ctx *gin.Context) {
	var userLoginInfo gonull.Nullable

	if rawData, err := ctx.GetRawData(); err != nil {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the your body")
		return
	} else {
		err = userLoginInfo.DecodeFromJSON(string(rawData))
		if err != nil {
			response.ErrorResponse(ctx, http.StatusBadRequest, err.Error())
			return
		}
	}

	// check the fields that needs to be provided
	if userLoginInfo.IsSet("Email") < gonull.EXIST {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the email")
		return
	}

	if userLoginInfo.IsSet("Password") < gonull.EXIST {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the email")
		return
	}

	email := strings.ToLower(userLoginInfo.GetString("Email"))
	passwd := userLoginInfo.GetString("Password")

	token, err := ctrl.UserUseCase.Login(email, passwd)
	if err != nil {
		response.ErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(ctx, token)
}

func (ctrl *UserController) UpdateUser(ctx *gin.Context) {
	var userUpdateInfo gonull.Nullable

	if rawData, err := ctx.GetRawData(); err != nil {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the your body")
		return
	} else {
		err = userUpdateInfo.DecodeFromJSON(string(rawData))
		if err != nil {
			response.ErrorResponse(ctx, http.StatusBadRequest, err.Error())
			return
		}
	}

	// check the fields that need to be provided
	if userUpdateInfo.IsSet("Email") < gonull.EXIST {
		response.ErrorResponse(ctx, http.StatusBadRequest, "please specify the email")
		return
	}

	// password cannot be null
	if userUpdateInfo.IsSet("Password") == gonull.NULLVALUE {
		response.ErrorResponse(ctx, http.StatusBadRequest, "password cannot be null")
		return
	}

	email := strings.ToLower(userUpdateInfo.GetString("Email"))

	// remove immutable fields
	userUpdateInfo.Delete("Email")

	err := ctrl.UserUseCase.UpdateUser(email, &userUpdateInfo)
	if err != nil {
		response.ErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(ctx, nil)
}
