package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Response BaseResponse
	Data     interface{} `json:",omitempty"`
}

type BaseResponse struct {
	Code int
	Mesg string
}

func SuccessResponse(c *gin.Context, data interface{}) {
	createResponse(c, 0, "SUCCESS", data)
}

func ErrorResponse(c *gin.Context, errorCode int, errorMesg string) {
	createBaseResponse(c, errorCode, errorMesg)
}

func createResponse(c *gin.Context, code int, mesg string, data interface{}) {
	baseResponse := BaseResponse{
		Code: code,
		Mesg: mesg,
	}

	response := Response{
		Response: baseResponse,
		Data:     data,
	}

	c.JSON(http.StatusOK, response)
}

func createBaseResponse(c *gin.Context, code int, mesg string) {
	baseResponse := BaseResponse{
		Code: code,
		Mesg: mesg,
	}
	response := Response{
		Response: baseResponse,
	}

	c.JSON(http.StatusOK, response)
}
