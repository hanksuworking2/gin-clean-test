package http

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"gin-clean-test/delivery/http/request"
	"gin-clean-test/delivery/http/response"
	"gin-clean-test/domain"

	zapLogger "go-utils/logger"
)

type EdgeController struct {
	EdgeUseCase domain.EdgeUsecase

	Logger *zapLogger.Logger
}

func NewEdgeController(edgeUseCase domain.EdgeUsecase, logger *zapLogger.Logger) *EdgeController {
	return &EdgeController{EdgeUseCase: edgeUseCase, Logger: logger}
}

func (ctrl *EdgeController) EdgeDeviceDetailApi(ctx *gin.Context) {
	var baseInfo request.BaseRequestInfo
	if err := ctx.ShouldBindJSON(&baseInfo); err != nil {
		response.ErrorResponse(ctx, http.StatusBadRequest, fmt.Errorf("please make sure the body format is correct, err=%s", err).Error())
		return
	}

	edges, err := ctrl.EdgeUseCase.EdgeDeviceDetailApi(baseInfo.Id)
	if err != nil {
		response.ErrorResponse(ctx, http.StatusInternalServerError, fmt.Errorf("failed to list edge devices, err=%s", err).Error())
		return
	}

	response.SuccessResponse(ctx, edges)
}
