package http

import (
	"github.com/gin-gonic/gin"

	zapLogger "go-utils/logger"

	"gin-clean-test/database"
	"gin-clean-test/delivery/http/middleware"
	mysqlRepository "gin-clean-test/repositories/mysql"
	redisRepository "gin-clean-test/repositories/redis"
	usecases "gin-clean-test/usecases"
)

func NewHTTPRouter(dbc *database.DataBaseConnCtrl, logger *zapLogger.Logger) (router *gin.Engine) {
	router = gin.New()
	router.Use(gin.Recovery(), middleware.CORSMiddleware(), middleware.RequestLoggerMiddleware(logger))

	/*----------Usecases----------*/
	userUseCase := usecases.NewUserUsecase(mysqlRepository.NewMysqlUserRepository(dbc.MySQL().DB()),
		redisRepository.NewRedisTokenRepository(dbc.Redis()))

	edgeUseCase := usecases.NewEdgeUsecase(mysqlRepository.NewMysqlEdgeRepository(dbc.MySQL().DB()))

	/*----------Controllers----------*/
	userController := NewUserController(userUseCase, logger)
	edgeController := NewEdgeController(edgeUseCase, logger)

	/*----------Routing----------*/

	needAuthenticate := router.Group("", middleware.NeedAuthenticateHandler(logger))
	{
		needAuthenticate.POST("/edge/device/detail", edgeController.EdgeDeviceDetailApi)

		needAuthenticate.PUT("/api/user", userController.UpdateUser)
		// needAuthenticate.POST("/api/logout", userController.Logout)
		// extend session time
		// needAuthenticate.POST("/api/touch", userController.Touch)
	}

	nonAuthenticate := router.Group("", middleware.NonAuthenticateHandler(logger))
	{
		nonAuthenticate.POST("/api/login", userController.Login)
		nonAuthenticate.POST("/api/register", userController.Register)
	}

	return
}
